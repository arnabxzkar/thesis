#!/bin/bash

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

cd "$DIR"

mkdir -p tables

for framework in tf-dr-n tf-ps-n cntk tf-ps-best tf-ps-best-strong cntk-strong hvd hvd-1024 cntk-1024 
do
    echo "Making tables for $framework..."
    scripts/table_single.py -f tests/$framework/*-imgpersec.data -o tables/ -t "Images per second for $framework"
    scripts/table_multi.py -f  tests/$framework/*-compcomm.data -o tables/ -t "Communication and computation time for $framework" \
        || scripts/table_single.py -f  tests/$framework/*-compcomm.data -o tables/ -t "Communication and computation time for $framework"
        
done
