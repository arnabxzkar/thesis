\chapter{Task Description}

This thesis compares the distributed behaviour of the CNTK and TensorFlow frameworks when training Deep Neural Networks for image classification. The specific networks are described in the following sections. Training is performed on the ILSRVC 2015 dataset \cite{imagenet}, otherwise known as ImageNet. 

\paragraph{AlexNet}

The AlexNet \cite{alexnet} network consists of 650,000 neurons and has around 60 million parameters. It has five convolutional layers and three fully connected ones. It was one of the first networks that significantly improved classification accuracy compared to traditional approaches. At the ILSVRC-2012 competition, AlexNet won with a top-5 error rate of 15.3\% \cite{ilsvrc2012}.

\begin{figure}[H]
	\caption{AlexNet topology}
	\includegraphics[width=0.99\textwidth]{images/alexnet2.png}
\end{figure}

\paragraph{VGG19}

VGG \cite{vgg} is a class of Deep Neural Networks. VGG19 consists of 19 layers, using only 3x3 convolutional filters and 2x2 max pooling layers. Due to the small size of the convolutional filters, the network can be made deeper, increasing its accuracy. VGG participated in the ILSVRC-2014 competition, where it achieved first place in the localisation and second in the classification categories~\cite{ilsvrc2014}.

\begin{figure}[H]
\caption{VGG16 topology. This topology is similar to VGG19, but has fewer layers}
\includegraphics[width=0.9\textwidth]{images/vgg16.png}
\end{figure}

VGG networks perform well on the ImageNet data, however, the large width of the individual convolutional layers makes training them very resource intensive. For this reason, VGG19 was included among the networks tested rather than the better performing VGG16.

\paragraph{ResNet-50 and ResNet-152}

ResNet \cite{resnet152} is also a class of Deep Neural Networks developed by the Microsoft Research team. ResNet-50 and ResNet-152 consist of 50 and 152 layers respectively, grouped in so-called \emph{Residual Blocks}. Each Residual Block learns a residual function with reference to its input. A network built from Residual Blocks is empirically easier to optimize, making it possible to build considerably deeper networks. This increased depth leads to a higher accuracy.

\begin{figure}[H]
	\begin{center}
		\caption{A single residual block. ResNet networks consist of a number of such blocks, chained together}
		\includegraphics[width=0.2\textwidth]{images/resnets.png}
	\end{center}
\end{figure}

\paragraph{Inception V3 and BN-Inception}

Inception \cite{inceptionv1}, otherwise known as GoogLeNet, is a kind of Deep Neural Networks first developded by Google. It aims to approximate a sparse convolutional network structure using dense operations in order to reduce the number of parameters, which in turn allows for a larger network depth.

To achieve this goal, the network is built from so-called \emph{inception modules} consisting of several convolution and pooling layers as well as a concatenation filter.

\begin{figure}[H]
	\caption{An inception module as used in the original Inception network}
	\centering
	\includegraphics[width=0.8\textwidth]{images/inceptionmodule.png}
\end{figure}

Inception V3 \cite{inceptionv3} is the third iteration of this kind of network. Its main advantage over the original is a decrease in computational complexity, which is achieved by factorizing the convolutions of the Inception module into even smaller convolutions. Once again, this allows for the network to be made deeper. On top of that, it adds normalization to the network, increasing its performance.

\begin{figure}[H]
	\caption{Inception V3 topology}
	\centering
	\includegraphics[width=\textwidth]{images/inception3.png}
\end{figure}

% mention that you did run the tensorflow one
Unfortunately, we were not able to run the CNTK implementation of Inception V3 on the Piz Daint supercomputer, as the default implementation used a number of input nodes not suitable for ImageNet. Instead, we performed tests using BN-Inception \cite{bn-inception}, a variant of the original Inception that adds batch normalization to the network. It is important to note that this architecture differs from Inception V3, so the results for those cannot be directly compared.

\section{Comparison of used networks}

The network architectures used for the tests cover a spectrum of different networks: AlexNet and VGG are wider, but less deep, whereas the Inception and particularly the ResNet family have slim and very deep networks that rely less on fully connected layers. VGG19 is the most computationally expensive network tested, whereas AlexNet is the cheapest, as can be seen in table ~\ref{table:flops}.

\begin{table}[H]
	\centering
	\caption{Comparison of networks under test}
	\label{table:flops}
	\begin{tabular}{r | r | r}
		     Network & FLOP per forward pass            & Number of parameters      \\ \hline
		     AlexNet & 729M \cite{resourcerequirements}  & 60M \cite{alexnet}        \\
		       VGG19 & 19600M \cite{resnet152}           & 144M \cite{vgg}           \\
		   ResNet-50 & 3922M \cite{resourcerequirements} & 0.85M \cite{resnet152}    \\
		  ResNet-152 & 11300M \cite{resnet152}           & 1.7M  \cite{resnet152}    \\
		Inception V3 & 5000M \cite{inceptionv3}          & 25M \cite{inceptionv3}    \\
		BN-Inception & 2000M \cite{convnetburden}        & 13.6M \cite{bn-inception}
	\end{tabular}
\end{table}

