\chapter{Experimental Setup}\label{experiments}

\section{Objectives}

The experiments aimed to measure performance of the CNTK and TensorFlow machine learning frameworks on a selection of convolutional neural networks. More concretely, the goal was to measure the wall clock time required for each minibatch processed as well as the time spent on communication. These measurements were taken for each node separately.

Two different kinds of experiment were performed: Weak scaling and strong scaling. For the former, the workload (i.e.\ the minibatch size) increases with the number of nodes in such a way that the workload per node remains constant. For the latter, the overall workload is constant, resulting in smaller workloads per node as the number of nodes increases.

\section{Tools for Performance Measurement}

CNTK and TensorFlow both contain built-in tools for measuring performance. 

In CNTK, an additional configuration parameter enables logging of the training time of each individual minibatch, with separate timing measurements for gradient aggregation. Each node produced its own log file, enabling a precise breakdown of communication versus computation time.

In TensorFlow, timing data was logged by an internal \emph{Supervisor} process and could later be programmatically evaluated. Using the Supervisor data, log files containing timing measurements per minibatch for each core were produced, not unsimilar to CNTK.

Unfortunately, TensorFlow does not measure communication time separately, thus rendering a precise breakdown of communication and computation time impossible. For that reason the ideal case, in which computation time is constant across node counts (for the weak scaling experiments), had to be assumed. For the strong scaling experiments, no distinction between computation and communication time for TensorFlow was made, as a similar assumption would likely not hold true in that case.

Both frameworks provide fine-tuned implementations of the tested networks. For all experiments, these implementations have been used with minor modifications in minibatch and epoch size.

\section{Parameters}

\paragraph{Minibatch size}

Across all weak scaling experiments, the minibatch size was set to 32 images per node ($\text{minibatch size} = 32 * \#\text{nodes}$). This was necessary to make the experiments comparable. Note that this introduces an ambiguity, since \emph{minibatch} could refer either to the overall minibatch or the partitioned minibatch per node. Henceforth, the term \emph{minibatch} is used for the overall minibatches, whereas the \emph{per-node minibatch} is always explicitly named.

For the strong scaling experiments, the minibatch size was set to 256 images, with one exception: For AlexNet, a  size of 1024 was chosen. A minibatch size of 256 is the default value for CNTK, and TensorFlow was adjusted to that for comparability. The exception for AlexNet is based on a paper by Alex Krizhevsky~\cite{weirdtrick}, who has empirically determined 1024 to be the best minibatch size for distributed AlexNet.

These batch sizes lead to the single-node experiments not running in most cases, because the single node was not able to keep the whole minibatch in memory and would crash with an out-of-memory error. The weak scaling experiments did not have this problem, as the minibatch size on the single node experiment was smaller.

\paragraph{Epoch size}

The epoch size was set to 100 minibatches, and training was run for one epoch. Since we are not concerned about accuracy, running for more than one epoch was not required. 

\paragraph{Parameter servers}

TensorFlow additionally allows to specify the number of parameter servers and the method for replicating parameters. Weak scaling experiments were performed for three combinations of these meta-parameters:

\begin{itemize}
	 \item One parameter server per node, with \emph{distributed\_replicated} setting (\emph{tf-dr-n})
	 \item One parameter server per node, with \emph{parameter\_server} setting (\emph{tf-ps-n})
	 \item Optimal number of parameter servers per node according to experiments performed in \cite{ebug}, with \emph{parameter\_server} setting (\emph{tf-ps-best}). The exact numbers are listed below.
\end{itemize}

Note that these parameter server numbers were empirically deemed to be optimal for training with the Inception V3 network, and might not necessarily be optimal for other networks.

\begin{table}[H]
	\caption{Parameter server counts for node counts}
	\label{table:tf-ps-count}
	\centering
	\begin{tabular}{r | r}
		\# nodes & \# PS \\
		       1 & 1                    \\
		       2 & 1                    \\
		       4 & 2                    \\
		       8 & 2                    \\
		      16 & 4                    \\
		      32 & 12
	\end{tabular}
\end{table}

With CNTK, these meta-parameters are not relevant, as it uses MPI reductions instead of parameter servers.

In the strong scaling experiments, the  \emph{tf-dr-n} and \emph{tf-ps-n} configurations were dropped because the weak scaling experiments showed that \emph{tf-ps-best} outperforms both of these configurations.

\section{Evaluation}

From the timing data obtained in the experiments, two principal measures for performance were derived: The average computation and communication time per minibatch and the number of images processed per second.

First, the average time per minibatch was calculated by simply taking the arithmetic mean of all time measurements for all the per-node minibatches across all cores. Since the nodes are run in parallel, the per-node time required for a per-node minibatch is on average the same as the overall time for a whole minibatch.

The average communication time (if available) was aggregated analogously. In cases where no communication time was measured, the average total time per minibatch for the single node experiment was used as an approximation for the weak scaling experiments. The average computation time is derived by subtracting the average communication time from the average total time per minibatch.

To calculate the images per second, it is sufficient to divide the minibatch size by the average total time per minibatch.

The plots in the following chapter show the accumulated results of the experiments. Plots for each framework are shown as well as direct comparisons between the frameworks for the same network. The AlexNet and VGG19 networks are plotted separately from the others, since the results differ considerably.

\subsection{Experimental Methodology}

It was originally intended to run the experiments for as many times as required for the 95\% confidence interval to be within 5\% of the mean, according to the rules on reporting performance results suggested by T. Hoefler and R. Belli~\cite{methodology}. Unfortunately, due to problems with the EasyBuild module system on Piz Daint, this was not possible. A change in this system caused both the TensorFlow and CNTK modules to no longer work, and sadly, there was not enough time to mitigate this issue. For that reason some of the experiments, especially for strong scaling, have rather few samples.

The raw data from which the plots were generated can be found in Appendix~\ref{appendix}. The measurement values and standard deviations presented there are rounded according to~\cite{reportmeans}. 

The error bars in the plots denote the 95\% confidence interval for a given run. Note that some of the values were derived from a single measurement, meaning that there is no confidence interval, whereas some values have a confidence interval that is small enough to be invisible in the plots. For that reason, the exact number of samples for each data point can also be found in Appendix~\ref{appendix}.

\subsection{Code}

The implementations of the individual neural networks for CNTK and TensorFlow were already provided and could be reused for this thesis. Some modifications to the available material were necessary to make it usable on the Piz Daint supercomputer. 

For TensorFlow, a benchmarking framework by Emanuele Bugliarello~\cite{ebug} could be reused, which was extended to include more networks. In addition, a script automating the Slurm task scheduling has been written.

For CNTK, a new benchmarking framework was created, automating the task scheduling and setting up the environment for CNTK to be able to run. 

To evaluate the log files, a number of scripts has been written to extract the timing data and generate the figures found in this thesis.

All code used for running and evaluating the benchmarks presented in this thesis can be found on the internet: 

\begin{itemize}
	\item \url{https://gitlab.com/HappyTetrahedron/tf-scripts}
	\item \url{https://gitlab.com/HappyTetrahedron/cscs-scripts}
	\item \url{https://gitlab.com/HappyTetrahedron/hvd-scripts}
	\item \url{https://gitlab.com/HappyTetrahedron/thesis}
\end{itemize}

