\chapter{Testing Environment}

All tests are run on the Piz Daint supercomputer \cite{pizdaint-web} located at the Swiss National Supercomputing Centre (CSCS). It is a hybrid Cray XC40/XC50 system providing GPU and Multicore compute nodes, of which only the GPU nodes were used for the experiments. Those are equipped with a 12-core Intel Xeon E5-2690 processor and an NVIDIA Tesla P100 GPU each. For inter-node communication a customized and highly for the CSCS supercomputes optimized implementation of the \emph{MPICH} library is provided.

Resources on Piz Daint are managed using the Slurm Workload Manager, which allocates nodes for every submitted job in a way that optimizes locality of the nodes assigned to it.

Piz Daint provides a parallel file system accessible on all compute nodes, called the \emph{scratch space}. It is extremely fast with a peak throughput of 138 GB per second~\cite{pizdaint-specs}, but it nonetheless is a shared resource and performance is affected by other processes accessing it at the same time.

Both CNTK and TensorFlow had to be compiled on Piz Daint to work on the Cray system. On top of that, the frameworks required a number of patches and minor modifications to be able to run. 

\section{Setup of the Frameworks on Piz Daint}

\paragraph{CNTK}

The results presented in this thesis were produced with a version of CNTK that was modified to use the MPICH MPI library instead of the default OpenMPI. This CNTK version was compiled on Piz Daint using the provided \emph{EasyBuild} framework for producing custom software modules.

The testing process submits one job per node count to Slurm. Each of these jobs trains all the networks to be tested, one after the other. This implies that the experiments for different node counts do not run on the same node configuration and might exhibit slightly different communication latencies. This has been accounted for by running the experiments multiple times on different node configurations.

Initially, a lot of effort was put into running CNTK in docker containers because it seemed to require fewer modifications to be able to run on Piz Daint. However, the container environment introduced a calculation overhead that affected the results. Since the root cause proved elusive, the initial results for CNTK had to be discarded.

\paragraph{TensorFlow} 

For TensorFlow, previous work by Emanuele Bugliarello \cite{ebug}, who has evaluated the performance of Distributed TensorFlow on Piz Daint before, came in useful. TensorFlow version 1.2.1 was built using the \emph{EasyBuild} build framework available on the supercomputer, enabling the compilation of TensorFlow for the Cray based Piz Daint system. In addition to the compiled TensorFlow library, a Python virtual environment was set up to install additional Python dependencies.

The testing process for TensorFlow submits one job per node count and network to Slurm. Each job trains only one of the networks to be tested. This once again implies that the node configuration for each job might vary, which was accounted for once again by running the experiments multiple times.

This process slightly differs from the process for CNTK, where for a given node count, the networks to be tested are run on the same allocation of nodes. This is because the launch process of Distributed TensorFlow requires a script with distinct parameters for each node.

\section{Methodology}

\paragraph{CNTK}

A single job for CNTK simply launches the CNTK binary with the appropriate network configuration using the \emph{srun} utility, a part of Slurm. This utility creates a job on all nodes within the current Slurm allocation. CNTK is thus started on each node and the individual instances automatically discover each other before they start training according to the supplied configuration.

Each node already produces a separate log file by default, so no additional configuration was necessary with respect to this.

\paragraph{TensorFlow}

A single job for TensorFlow performs the following actions: First, a series of bash scripts is automatically generated, one per node. Each script launches TensorFlow with proper parameters for its node. These parameters include the hostnames of all other participating nodes and whether a node hosts a parameter server. Additionally, the scripts redirect TensorFlow's log output to individual log files, making a seperate script per node necessary. 

Second, the job causes each node to execute its assigned bash script, starting TensorFlow on each node separately. The TensorFlow instances connect to a global session, whose task it is to orchestrate the training process.

The TensorFlow parameter servers, which take care of replicating the model parameters, are run on the same nodes as the workers (which do the actual training). The launcher script runs the parameter server and worker in seperate processes ensuring that the parameter server runs on the node's CPU while the worker runs on the GPU.

Use of the \emph{srun} utility was not possible for TensorFlow, since different parameters were required for each node.

\section{Caveats}

For (at the time of writing) unknown reasons, the TensorFlow tests did not always run reliably. At times, the TensorFlow session would not finish and the test would time out without producing any log files. Interestingly, some networks (such as VGG19) were affected more often than others. Also, the phenomenon appeared more frequently during times where Piz Daint was more intensively used.

 Due to this, there is not the same number of experiment runs for every network and node count on TensorFlow and some results are missing entirely. 

Overall, the setup process for distributed TensorFlow is complex and error prone, in steep contrast with the ease and reliability of setting up regular TensorFlow. Compared to CNTK, this process could be heavily improved.
