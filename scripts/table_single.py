#!/usr/bin/env python3

# Plots a grouped stacked bar graph for arbitrary data supplied as a yaml file.
# The data is structured as follows:
#
# meta:
#  xlabel: "My x axis"
#  ylabel: "My y axis"
#  legendsuffix: "Bar"
#  segmentlabels:
#    - "label 1"
#    - "label 2"
# group1:
#   bar1:
#     - value1
#     - value2
#   bar2:
#     - value1
#     - value2
# group2:
#   bar1:
#     - value1
#     - value2
#   bar2:
#     - value1
#     - value2
#
# Bars within a group consist of as many segments as there are values.
# There have to be the same number of values for every bar, and the same number of bars for every group.
# The 'meta' section contains some parameters to customize the plot, such as axis labels.
#

import common
import numpy

opts = common.get_options()
(metadata, data) = common.get_data(opts)

tables = {}
for i,group in enumerate(sorted(data.keys())):
    table = []

    for bar in data[group].keys():
        tabledata = {}

        has_samples = data[group][bar][0] != 0

        tabledata[metadata.get('legendsuffix', "Label")] = bar

        if has_samples:
            nsamples = len(data[group][bar])
            mean = numpy.mean(data[group][bar])
            std = numpy.std(data[group][bar])
            ci = common.zval * std / float(nsamples)
            (mean, std, ci) = common.fix_precision(mean, std, ci, nsamples)

        tabledata["# samples"] = nsamples if has_samples else 0
        tabledata[metadata.get('ylabel', "Mean")] = mean if has_samples else '-'
        tabledata["standard error"] = std if has_samples else '-'
        tabledata["95% CI"] = "+- %g" % ci if has_samples else '-'

        table.append(tabledata)

    nsamples = [ d['# samples'] <= 1 for d in table ]
    if all(nsamples):
        for d in table:
            d.pop('standard error')
            d.pop('95% CI')

    tables[group] = table

common.savetables(opts, tables)
