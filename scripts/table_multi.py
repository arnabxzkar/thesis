#!/usr/bin/env python3

# The data is structured as follows:
#
# meta:
#  xlabel: "My x axis"
#  ylabel: "My y axis"
#  legendsuffix: "Bar"
#  segmentlabels:
#    - "label 1"
#    - "label 2"
# group1:
#   bar1:
#     - value1
#     - value2
#   bar2:
#     - value1
#     - value2
# group2:
#   bar1:
#     - value1
#     - value2
#   bar2:
#     - value1
#     - value2
#
# Bars within a group consist of as many segments as there are values.
# There have to be the same number of values for every bar, and the same number of bars for every group.
# The 'meta' section contains some parameters to customize the plot, such as axis labels.
#

import common
import numpy

opts = common.get_options()
(metadata, data) = common.get_data(opts)

tables = {}
for i,group in enumerate(sorted(data.keys())):
    table = []

    samples = []
    for bar in data[group].keys():
        has_samples = data[group][bar][0][0] != 0
        nsamples = len(data[group][bar][0]) if has_samples else 0
        samples.append(nsamples <= 1)

    needs_std = True
    if all(samples):
        needs_std = False

    for bar in data[group].keys():
        tabledata = {}

        has_samples = data[group][bar][0][0] != 0

        tabledata[metadata.get('legendsuffix', "Label")] = bar
        tabledata["# samples"] = len(data[group][bar][0]) if has_samples else 0
        for i,ys in enumerate(data[group][bar]):
            label = "Group %d" % i
            if metadata.get('tablecolumns'):
                label = metadata['tablecolumns'][i]
            if has_samples:
                nsamples = len(ys)
                mean = numpy.mean(ys)
                std = numpy.std(ys)
                ci = common.zval * std / float(nsamples)
                mean, std, ci = common.fix_precision(mean, std, ci, nsamples)

            tabledata[label] = mean if has_samples else '-'
            if needs_std:
                tabledata["%s std" % label] = std if has_samples else '-'
                tabledata["%s 95%% CI" % label] = "+- %g" % ci if has_samples else '-'

        table.append(tabledata)

    tables[group] = table

common.savetables(opts, tables)
