#!/usr/bin/env python3


import yaml
from optparse import OptionParser
from tabulate import tabulate
import numpy
import math

numdigits = 5
alphabet = ['a','b','c','d','e','f']
zval = 1.96 # z* value for 95% CI


def get_options():
    parser = OptionParser()
    parser.add_option("-f", "--file", dest="filename", action="store", type="string", help="Filename of data file")
    parser.add_option("-o", "--outdir", dest="dirname", action="store", type="string", help="Output directory")
    parser.add_option("-O", "--tableoutdir", dest="tabledirname", action="store", type="string", default=None, help="Output directory for tables")
    parser.add_option("-t", "--title", dest="title", default="", action="store", type="string", help="Title")
    parser.add_option("-s", "--suffix", dest="custsuffix", default=None, action="store", type="string", help="Custom suffix")
    parser.add_option("-F", "--format", dest="format", default="png", action="store", type="string", help="Output format")
    parser.add_option("-l", "--logscale", dest="logscale", default=False, action="store_true", help="Set to use logarithmic scale for y axis")
    parser.add_option("-j", "--hidelegends", dest="hidelegends", default=False, action="store_true", help="Set to hide legends")
    parser.add_option("-p", "--pick", dest="picks", default=None, action="store", type="string", help="Comma-separated list of keys to pick for the plot")
    parser.add_option("-i", "--indices", dest="inds", default=None, action="store", type="string", help="Comma-separated list of row indices to pick for the plot (has to be in order)")
    parser.add_option("-T", "--fitfun", dest="fitfun", default=None, action="store", type="string", help="Function to fit")

    (opts, args) = parser.parse_args()
    if not opts.filename:
        parser.error("Filename is required")

    opts.outname = opts.filename.split('.')[0]
    if opts.dirname:
        opts.outname = opts.outname.split('/')[-1]
    else:
        opts.dirname = '.'

    if not opts.tabledirname:
        opts.tabledirname = opts.dirname

    if opts.picks:
        opts.pick = opts.picks.split(',')
    else:
        opts.pick = None

    if opts.inds:
        opts.indices = opts.inds.split(',')
    else:
        opts.indices = None

    if opts.custsuffix:
        opts.custsuffix = '-%s' % opts.custsuffix
    else:
        opts.custsuffix = ""

    return opts


# Load data
def get_data(opts):
    with open(opts.filename) as ymlfile:
        data = yaml.load(ymlfile)
        ymlfile.close()

    metadata = data.pop('meta', None)

    popme = []
    if opts.pick:
        for key in data.keys():
            if not key in opts.pick:
                popme.append(key)

    for key in popme:
        data.pop(key)

    return (metadata, data)

def get_fitfun(opts):
    if not opts.fitfun:
        return None
    if opts.fitfun == "log":
        return lambda x, a, b: a + b * numpy.log2(x)
    if opts.fitfun == "inverse":
        return lambda x, a, b: a + b / x
    return None

def get_init(opts):
    if not opts.fitfun:
        return []
    if opts.fitfun == "log":
        return [0,1]
    if opts.fitfun == "inverse":
        return [1,1]
    return []

def filter_list(opts, l):
    if opts.indices:
        ret = [ l[int(i)] for i in opts.indices ]
    else:
        ret = l
    return ret


def use_index(opts, index):
    if not opts.indices:
        return True

    for i in opts.indices:
        if index == int(i):
            return True

    return False


def fix_precision(mean, std, ci, nsamples): 
    # Fixes precision of mean and std according to https://arxiv.org/pdf/1301.1034.pdf
    if std == 0:
        if mean == 0:
            return (mean, std, ci)
        # Well, what do we do now?
        mean_sig = -int(math.floor(math.log10(abs(mean))))
        new_mean = round(mean, mean_sig + 1)
        return (new_mean, std, ci)

    std_sig = -int(math.floor(math.log10(abs(std))))
    new_mean = round(mean, std_sig)

    supp_std = 1 if nsamples <= 6 else (
               2 if  nsamples <= 100 else 3)

    new_std = round(std, std_sig - supp_std + 2)
    new_ci = round(ci, std_sig - supp_std + 2)

    return (new_mean, new_std, new_ci)


def get_colormap():
     return [[0,0.4470,0.7410],[0.8500,0.3250,0.0980],[0.9290,0.6940,0.1250],[0.52,0.91,0.54],[0.74,0.18,0.5410],[0.9500,0.8550,0.3980],[0.2500,0.6250,0.2980],[0.1990,0.9440,0.9550],[0.2,0.2,0.7],[0.96,0.16,0.2],[.99999,0.7970,0.25]]

def remove_outliers(ys):
    mean = numpy.mean(ys)
    std = numpy.std(ys)
    new_ys = [ x for x in ys if abs(mean - x) <= std * 1.5 ]
    return new_ys


def save(opts, suffix, plt):
    plt.savefig("%s/%s-%s%s%s-fig.%s" % (opts.dirname, opts.outname, suffix, opts.custsuffix, "-log" if opts.logscale else "", opts.format), bbox_inches='tight')


def savetables(opts, tables):
    with open("%s/%s%s.tex" % (opts.tabledirname, opts.outname, opts.custsuffix), 'w') as outfile:
        for table in tables.keys():
            print('''\\begin{table}[H]
                \\centering
                \\caption{%s - %s}
                \\label{table:%s-%s%s}''' % (opts.title, table.replace('_', '\\_'), opts.outname, table.replace('_', ''), opts.custsuffix), file=outfile)
            print('''\\resizebox{%
                \\ifdim\\width>\\textwidth
                     \\textwidth
                \\else
                    \\width
                \\fi}{!}{%''', file=outfile)
            print(tabulate(tables[table], headers='keys', tablefmt='latex'), file=outfile)
            print('''}
                \end{table}''', file=outfile)
        outfile.close()

