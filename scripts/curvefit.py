#!/usr/bin/env python3

# Plots a grouped stacked bar graph for arbitrary data supplied as a yaml file.
# The data is structured as follows:
#
# meta:
#  xlabel: "My x axis"
#  ylabel: "My y axis"
#  legendsuffix: "Bar"
#  segmentlabels:
#    - "label 1"
#    - "label 2"
# group1:
#   bar1:
#     segment1:
#       - value1
#       - value2
#     segment2:
#       - value1
#       - value2
#   bar2:
#     segment1:
#       - value1
#       - value2
#     segment2:
#       - value1
#       - value2
# group2:
#   bar1:
#     segment1:
#       - value1
#       - value2
#     segment2:
#       - value1
#       - value2
#   bar2:
#     segment1:
#       - value1
#       - value2
#     segment2:
#       - value1
#       - value2
#
# Bars within a group consist of as many segments as there are segment sections.
# There have to be the same number of segments for every bar, and the same number of bars for every group.
# The 'meta' section contains some parameters to customize the plot, such as axis labels.
#

import common
import numpy
import scipy.optimize as optimize
import math
import matplotlib.pyplot as plt
import matplotlib.patches as mpat

suffix = "fitfun"

opts = common.get_options()
(metadata, data) = common.get_data(opts)

# List of colors to be used for bars within a group
colormap=common.get_colormap()

def setuplegend(ax, row):
    if opts.logscale:
        ax.set_yscale('log')
    # Set up axis labels as specified in the data
    ax.set_ylabel(metadata.get('ylabel') if metadata != None else '')
    ax.set_xlabel(metadata.get('xlabel') if metadata != None else '') 

    if not opts.hidelegends:
    # create proxy artists to use as "fake" legend entries for the bars
    # This is essentially a list of "patches", each with a color and a string label
        bar_legend_handles = [mpat.Patch(color=color, label=str(indices) + " " + str(metadata.get('legendsuffix') if metadata != None else '') ) for indices,color in zip(list(data.values())[0].keys(), colormap)]

    # similarly create legend for the segments if required
        segment_legend = None
        if metadata != None and metadata.get('segmentlabels') != None:
            # proxy artists
            segmentlabels = [metadata.get('segmentlabels')[row]]
            segment_legend_handles = [mpat.Patch(color='k', alpha=float(i+1)/float(len(segmentlabels)), label=str(label)) for i,label in enumerate(reversed(segmentlabels))]
            # create segment legend
            segment_legend = ax.legend(handles=segment_legend_handles, fancybox=True)
            # explicitly add legend to axis so we can add a second one later
            ax.add_artist(segment_legend)


        # create bar legend
        bar_legend = ax.legend(handles=bar_legend_handles, loc='center left', bbox_to_anchor=(1,0.5), fancybox=True)

# Iterate over all groups
table = []
for i,group in enumerate(sorted(data.keys())):
    means = []
    stds = []
    cis = []
    # for each segment...
    for (row, _) in enumerate(next(iter(data[group].values()))):
        # Get all measurements for this segments for every group
        if common.use_index(opts, row):

            # first comp time, then comm time
            perbar_lists = [ x[row] for x in data[group].values() if row < len(x)]
            table_entry = {}

            # Calculate mean and std for this segment 
            meanlist = [numpy.mean(x) for x in perbar_lists]
            stdlist = [numpy.std(x) for x in perbar_lists]
            nsamples = [len(x) for x in perbar_lists]
            cilist = [common.zval * stdlist[i] / math.sqrt(nsamples[i]) for i in range(0, len(stdlist))]

            # Number of bars in this group
            nbars = len(meanlist)

            f = plt.figure()
            ax = f.add_axes((.1,.4,.8,.5))

            x = [int(key) for key in data[group].keys()]

            ax.set_xticks(x)

            ax.bar(x, meanlist, 1, color=colormap, edgecolor='k', alpha=1, yerr=cilist)

            x = [int(key) for i,key in enumerate(data[group].keys()) if meanlist[i] > 0]
            meanlist = [mean for mean in meanlist if mean > 0]

            fun = common.get_fitfun(opts)
            initial = common.get_init(opts)

            try:
                curve = optimize.curve_fit(fun, x, meanlist, initial)
            except TypeError:
                # not enough stuff to fit a curve, skip
                print("Warning: Not enough data points to fit function. No plot was generated.")
                continue
                
            meanofmeans = numpy.mean(meanlist)

            residuals = [ mean - fun(xval, *curve[0]) for mean,xval in zip(meanlist, x)]
            simple_residuals = [ mean - meanofmeans for mean in meanlist ]

            sum_squared_residuals = 0
            simple_squared_residuals = 0

            for res in residuals:
                sum_squared_residuals += res * res
            for sres in simple_residuals:
                simple_squared_residuals += sres * sres

            table_entry[''] = group
            table_entry['SSE'] = sum_squared_residuals
            table_entry['RSE'] = sum_squared_residuals / simple_squared_residuals
            for param,value in zip(common.alphabet, curve[0]):
                table_entry['Parameter %s' % param] = value

            table.append(table_entry)

            ax.set_title(group)

            linspace = numpy.linspace(1,32,32)
            ax.plot(linspace, fun(linspace, *curve[0]), 'r')

            ax.set_ylim([0, max(meanlist) + 0.1 * max(meanlist)])

            setuplegend(ax, row)

            common.save(opts, suffix + '-' + group, f)

if table:
    common.savetables(opts, {'' : table})
