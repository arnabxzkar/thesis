#!/bin/bash

file="$1"
mbsize=-32

shopt -s extglob

rm -f $file

cat <<EOT >> $file
meta:
    ylabel: "Images per second"
    xlabel: ""
    legendsuffix: "Nodes"
EOT


for net in $( ls */*/!(*.rank*) | sed 's/.*\///' | sort | uniq )
do
    echo "$net:" | sed 's/Log_//' >> $file

    for numcores in $( ls */ | grep -e '^[0-9]' | sort -n | uniq )
    do
        echo "    $numcores": >> $file

        ips=()
        for measurement  in */
        do 
            for log in $( ls ${measurement}${numcores}/${net}* )
            do
                # Average time/mb for each core
                cat $log | grep -o 'time = [^s]*' | awk '{ SUM += $3 ; CNT += 1 } END { print (CNT > 0) ? SUM/CNT : 0 }' >> ${measurement}$numcores.avg
                mbsize="$( cat $log | grep bsize | sed 's/.*=//' )"

            done

            # Average time/mb over all cores
            avg="$( awk '{ SUM += $1 ; CNT += 1 } END { print SUM/CNT }' ${measurement}$numcores.avg )"

            if ! [[ $avg == 0 ]]
            then
                # Images per second = MB size / average time per MB
                ips+=("$( echo "($mbsize / $avg)" | bc -l )")
            fi

            rm ${measurement}$numcores.avg

        done

        if [[ -z "$ips" ]]
        then
            echo "        - 0" >> $file
        fi

        for i in "${ips[@]}"
        do
            echo "        - $i" >> $file
        done

    done

done
