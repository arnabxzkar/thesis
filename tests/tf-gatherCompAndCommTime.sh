#!/bin/bash

file="$1"
mbsize=32

shopt -s extglob

rm -f $file

cat <<EOT >> $file
meta:
    ylabel: "Time per node and MB [s]"
    xlabel: ""
    legendsuffix: "Nodes"
    segmentlabels:
        - "Computation time (assumed)"
        - "Communication time"
    tablecolumns:
        - "Comp. time"
        - "Comm. time"
EOT


for net in $( ls */ | grep -v results | grep -v tf | grep -v hvd | sort | uniq )
do
    echo "$net:" | sed 's/Log_//' >> $file

    for numcores in $( ls */$net/ | grep -e '^[0-9]' | sort -n | uniq )
    do
        echo "    $numcores": >> $file
        avgs=()
        comps=()

        for measurement in */
        do 
            for log in ${measurement}${net}/${numcores}/worker*
            do
                if [[ -f $log ]] && cat $log | grep 'on average' > /dev/null
                then
                    # Average time/mb for each core
                    cat $log | grep 'on average' | awk '{ SUM += $8 ; CNT += 1 } END { print SUM/CNT }' >> ${measurement}$numcores.avg
                fi
            done

            if [[ -f ${measurement}$numcores.avg ]]
            then
                # Average time/mb over all cores
                avg="$( awk '{ SUM += $1 ; CNT += 1 } END { print SUM/CNT }' ${measurement}$numcores.avg )"

                # Single core performance = computation time
                comp="$( grep 'on average' ${measurement}${net}/1/worker* | awk '{ SUM += $8 ; CNT += 1 } END { print SUM/CNT }' )"

                avgs+=("$avg")
                comps+=("$comp")

                rm ${measurement}$numcores.avg
            fi

        done

        echo "        - " >> $file

        if [ -z "$comps" ]
        then
            echo "            - 0" >> $file
        fi

        for i in ${!comps[@]}
        do
            echo "            - ${comps[$i]}" >> $file
        done

        echo "        - " >> $file

        if [ -z "$avgs" ]
        then
            echo "            - 0" >> $file
        fi

        for i in ${!avgs[@]}
        do
            echo -n "            - " >> $file
            echo "print(${avgs[$i]} - ${comps[$i]})" | python >> $file
        done

    done

done
