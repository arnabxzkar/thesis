#!/usr/bin/env python3

import yaml
import sys
from optparse import OptionParser

def get_data(filename, key):
    with open(filename) as ymlfile:
        data = yaml.load(ymlfile)
        ymlfile.close()
    relevant = data.pop(key, None)
    if relevant is None:
        print("Invalid key")
        sys.exit(1)
    return relevant

pars = OptionParser()
pars.add_option('-d', '--dirs', dest='dirs', action='store', type='string')
pars.add_option('-k', '--keys', dest='keys', action='store', type='string')
pars.add_option('-p', '--prefixes', dest='prefixes', action='store', type='string')
pars.add_option('-c', '--comparison', dest='comp', action='store', type='string')
(opts, args) = pars.parse_args()

keys = opts.keys.split(',')
dirs = opts.dirs.split(',')
prefixes = opts.prefixes.split(',')

datas = []

for k,d,p in zip(keys, dirs, prefixes):
    datas.append({
        'img': get_data('%s/%s-imgpersec.data' % (d, p), k),
        'cc': get_data('%s/%s-compcomm.data' % (d, p), k),
        'label': p,
    })

img_data = {}
for data in datas:
    for key in data['img'].keys():
        img_data[key] = {}

for key in img_data.keys():
    for data in datas:
        if key in data['img'].keys():
            img_data[key][data['label']] = data['img'][key]
        else:
            img_data[key][data['label']] = [0]

img_data['meta'] = {
    'ylabel': 'Images per second',
    'xlabel': 'Nodes',
    'legendsuffix': '',
    'title': 'Comparison for %s' % opts.comp,
}


cc_data = {}
for data in datas:
    for key in data['cc'].keys():
        cc_data[key] = {}

for key in cc_data.keys():
    for data in datas:
        if key in data['cc'].keys():
            cc_data[key][data['label']] = data['cc'][key]
        else:
            cc_data[key][data['label']] = [[0],[0]]

cc_data['meta'] = {
    'ylabel': 'Time per core and MB [s]',
    'xlabel': 'Nodes',
    'legendsuffix': '',
    'title': 'Comparison for %s' % opts.comp,
    'segmentlabels': [
        'Computation time',
        'Communication time',
    ]
}

with open('compare-imgpersec-%s.data' % opts.comp, 'w') as ymlfile:
    yaml.dump(img_data, ymlfile, default_flow_style=False)

with open('compare-compcomm-%s.data' % opts.comp, 'w') as ymlfile:
    yaml.dump(cc_data, ymlfile, default_flow_style=False)
