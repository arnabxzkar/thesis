#!/bin/bash

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
cd "$DIR"

if [ -z $1 ]
then
    for dir in */
    do
        echo "Processing $dir..."
        pushd "$dir" > /dev/null
        echo "    Computation and communication time..."
        ./gatherCompAndCommTime.sh
        echo "    Images per second..."
        ./gatherImagesPerSecond.sh
        popd > /dev/null
    done
fi

echo "Processing comparisons..."
echo "    AlexNet..."
./generateComparisonData.py -d cntk,tf-dr-n,tf-ps-n,tf-ps-best -k AlexNet,alexnet,alexnet,alexnet,alexnet -p cntk,tf-dr-n,tf-ps-n,tf-ps-best,tf-ps-best-fixed -c alexnet

echo "    ResNet152..."
./generateComparisonData.py -d cntk,tf-dr-n,tf-ps-n,tf-ps-best -k ResNet_152,resnet152,resnet152,resnet152,resnet152 -p cntk,tf-dr-n,tf-ps-n,tf-ps-best,tf-ps-best-fixed -c resnet152

echo "    ResNet50..."
./generateComparisonData.py -d cntk,tf-dr-n,tf-ps-n,tf-ps-best -k ResNet_50,resnet50,resnet50,resnet50,resnet50 -p cntk,tf-dr-n,tf-ps-n,tf-ps-best,tf-ps-best-fixed -c resnet50

echo "    VGG19..."
./generateComparisonData.py -d cntk,tf-dr-n,tf-ps-n,tf-ps-best -k VGG19,vgg19,vgg19,vgg19,vgg19 -p cntk,tf-dr-n,tf-ps-n,tf-ps-best,tf-ps-best-fixed -c vgg19

echo "    Inception..."
./generateComparisonData.py -d cntk,tf-dr-n,tf-ps-n,tf-ps-best -k BN-Inception,inception3,inception3,inception3,inception3  -p cntk,tf-dr-n,tf-ps-n,tf-ps-best,tf-ps-best-fixed -c 'inception3 and bn-inception'

echo "    Inception TF..."
./generateComparisonData.py -d tf-dr-n,tf-ps-n,tf-ps-best -k inception3,inception3,inception3,inception3  -p tf-dr-n,tf-ps-n,tf-ps-best,tf-ps-best-fixed -c 'inception3 on tensorflow'

echo "Processing comparisons (strong)..."
echo "    AlexNet..."
./generateComparisonData.py -d cntk-strong,tf-ps-best-strong,tf-ps-n-strong -k AlexNet,alexnet,alexnet -p cntk-strong,tf-ps-best-strong,tf-ps-n-strong -c alexnet-strong

echo "    ResNet152..."
./generateComparisonData.py -d cntk-strong,tf-ps-best-strong,tf-ps-n-strong -k ResNet_152,resnet152,resnet152 -p cntk-strong,tf-ps-best-strong,tf-ps-n-strong -c resnet152-strong

echo "    ResNet50..."
./generateComparisonData.py -d cntk-strong,tf-ps-best-strong,tf-ps-n-strong -k ResNet_50,resnet50,resnet50 -p cntk-strong,tf-ps-best-strong,tf-ps-n-strong -c resnet50-strong

echo "    VGG19..."
./generateComparisonData.py -d cntk-strong,tf-ps-best-strong,tf-ps-n-strong -k VGG19,vgg19,vgg19 -p cntk-strong,tf-ps-best-strong,tf-ps-n-strong -c vgg19-strong

echo "    Inception..."
./generateComparisonData.py -d cntk-strong,tf-ps-best-strong,tf-ps-n-strong -k BN-Inception,inception3,inception3 -p cntk-strong,tf-ps-best-strong,tf-ps-n-strong -c 'inception3-and-bn-inception-strong'



echo "Processing comparisons (x)..."
echo "    CNTK..."
for i in AlexNet ResNet_50 ResNet_152 BN-Inception VGG19
do
    ./generateComparisonData.py -d cntk,cntk-strong -k $i,$i -p cntk,cntk-strong -c "$i on cntk weak vs strong"
done

echo "    TensorFlow..."
for i in alexnet resnet50 resnet152 inception3 vgg19
do
    ./generateComparisonData.py -d tf-ps-best,tf-ps-best-strong -k $i,$i -p tf-ps-best,tf-ps-best-strong -c "$i tf-ps-best weak vs strong"
    ./generateComparisonData.py -d tf-ps-n,tf-ps-n-strong -k $i,$i -p tf-ps-n,tf-ps-n-strong -c "$i tf-ps-n weak vs strong"
done

echo "Processing comparisons (hvd)..."
echo "    AlexNet..."
./generateComparisonData.py -d cntk,tf-dr-n,tf-ps-n,tf-ps-best,hvd -k AlexNet,alexnet,alexnet,alexnet,alexnet -p cntk,tf-dr-n,tf-ps-n,tf-ps-best,hvd -c "alexnet with Horovod"

echo "Processing comparisons (1024)..."

echo "    AlexNet..."
./generateComparisonData.py -d cntk-1024,hvd-1024 -k AlexNet,alexnet -p cntk-1024,hvd-1024 -c "alexnet with 1024 nodes"

echo "    ResNet-50..."
./generateComparisonData.py -d cntk-1024,hvd-1024 -k ResNet_50,resnet50 -p cntk-1024,hvd-1024 -c "ResNet-50 with 1024 nodes"
